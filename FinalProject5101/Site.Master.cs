﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject5101
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataView pageview = getheadermenu();

            if (pageview == null)
            {
                return;
            }
            foreach (DataRowView rowView in pageview)
            {
                DataRow row = rowView.Row;
                headernavigation.InnerHtml += "<li><a runat='server' href='/DisplayWebPage.aspx?webpageid=" + row["webpageid"] + "'>" + row["pagetitle"] + "</a></li>";
            }

        }
        protected DataView getheadermenu()
        {
            string query = "select wp.webpageid, wp.pagetitle from webpages wp " +
                "full join webpagecontent wpc " +
                "on wp.webpageid = wpc.webpageid " +
                "where wpc.webpagestatus = 'Published'";

            managepage_con.SelectCommand = query;

            DataView ManagePageView = (DataView)managepage_con.Select(DataSourceSelectArguments.Empty);

            if (ManagePageView.ToTable().Rows.Count < 1)
            {
                return null;
            }
            return ManagePageView;

        }
    }
}