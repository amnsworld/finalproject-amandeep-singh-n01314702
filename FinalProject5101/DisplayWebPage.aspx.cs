﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject5101
{
    public partial class DisplayWebPage : System.Web.UI.Page
    {
        public int webpageid
        {
            get { return Convert.ToInt32(Request.QueryString["webpageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo1(webpageid);
            if (pagerow == null)
            {
                WebPageTitle.InnerHtml = "No Page Found.";
                return;
            }
            WebPageTitle.InnerHtml = pagerow["pagetitle"].ToString();
            WebPageAuthor.InnerHtml = pagerow["pageauthor"].ToString();
            WebPageCreatedOn.InnerHtml = pagerow["createdon"].ToString();
            WebPageContent1.InnerHtml = pagerow["webpagecontentone"].ToString();
            WebPageContent2.InnerHtml = pagerow["webpagecontenttwo"].ToString();
            

        }
       
        protected DataRowView getPageInfo1(int id)
        {
            string query = "select wp.*,wpc.* from webpages wp full join webpagecontent wpc on wp.webpageid = wpc.webpageid where wpc.webpageid =" + webpageid.ToString();
            page_query1.SelectCommand = query;
            DataView pageview = (DataView)page_query1.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }
    }
}