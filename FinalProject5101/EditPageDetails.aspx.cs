﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject5101
{
    public partial class EditPageDetails : System.Web.UI.Page
    {
        public int webpageid
        {
            get { return Convert.ToInt32(Request.QueryString["webpageid"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(webpageid);
            if (pagerow == null)
            {
                heading.InnerHtml = "No Page Found.";
                return;
            }
            heading.InnerHtml = "Edit - " + pagerow["pagetitle"];
            pageTitle.Text = pagerow["pagetitle"].ToString();
            pageAuthorName.Text = pagerow["pageauthor"].ToString();
            PageContentOne.Text = pagerow["webpagecontentone"].ToString();
            PageContentTwo.Text = pagerow["webpagecontenttwo"].ToString();
            if (pagerow["webpagestatus"].ToString() == "Published")
            {
                pageStatus.Checked = true;
            }
            else
            {
                pageStatus.Checked = false;
            }
        }

        protected DataRowView getPageInfo(int id)
        {
            string query = "select wp.pagetitle, wp.pageauthor, wpc.webpagecontentone, wpc.webpagecontenttwo, wpc.webpagestatus from webpages wp" +
                " full join webpagecontent wpc on wp.webpageid = wpc.webpageid " +
                "where wp.webpageid=" + webpageid.ToString();

            page_query.SelectCommand = query;

            DataView pageview = (DataView)page_query.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;

        }

        protected void EditPage(object sender, EventArgs e)
        {
            string title = pageTitle.Text;
            string author = pageAuthorName.Text;
            string content1 = PageContentOne.Text;
            string content2 = PageContentTwo.Text;
            string Status = "UnPublished";
            if (pageStatus.Checked == true)
            {
                Status = "Published";
            }
            string UpdatedOn = DateTime.Today.ToString("dd/MM/yyyy");


            string updateWebpage = "Update webpages set pagetitle='" + title + "'," + " pageauthor='" + author + "', modifiedon='" + UpdatedOn +
                "' where webpageid=" + webpageid.ToString();
            page_query.UpdateCommand = updateWebpage;
            page_query.Update();

            string updateWebpageContent = "Update webpagecontent set webpagecontentone='" + content1 + "'," +
                " webpagecontenttwo='" + content2 + "', webpagestatus='" + Status + "' where webpageid=" + webpageid.ToString();
            page_query.UpdateCommand = updateWebpageContent;
            page_query.Update();

            result.InnerHtml = result.InnerHtml = "<div class='alert alert-success' role='alert'>Success! Page status has been updated. </div><br><a href='ManagePages.aspx'>";
        }
       
    }
}