﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject5101
{
    public partial class ManagePages : System.Web.UI.Page
    {
        private string basequery = "SELECT webpages.webpageid,pagetitle as 'Page Title', pageauthor as 'Author',webpagecontent.webpagestatus as 'Status', convert(varchar, createdon,106) as 'Created On', convert(varchar, modifiedon,106) as 'Modified On' FROM webpages LEFT JOIN webpagecontent on webpages.webpageid = webpagecontent.webpageid";

        protected void Page_Load(object sender, EventArgs e)
        {
            manage_pages.SelectCommand = basequery;
            webpage_list.DataSource = WebPages_Manual_Bind(manage_pages);
            webpage_list.CssClass = "table"; ///bootstrap table 

            //BoundColumn w_id = new BoundColumn();
            //w_id.DataField = "webpageid";
            //w_id.HeaderText = "ID";
            //w_id.Visible = false;
            //webpage_list.Columns.Add(w_id);


            //BoundColumn page_title = new BoundColumn();
            //page_title.DataField = "Page Title";
            //page_title.HeaderText = "Page Title";
            //webpage_list.Columns.Add(page_title);

            //BoundColumn author = new BoundColumn();
            //author.DataField = "Created On";
            //author.HeaderText = "Created On";
            //webpage_list.Columns.Add(author);

            //BoundColumn modifiedon = new BoundColumn();
            //modifiedon.DataField = "Modified On";
            //modifiedon.HeaderText = "Modified On";
            //webpage_list.Columns.Add(modifiedon);

            //BoundColumn edit = new BoundColumn();
            //edit.DataField = "Edit";
            //edit.HeaderText = "Action";
            //webpage_list.Columns.Add(edit);

            //ButtonColumn delstudent = new ButtonColumn();
            //delstudent.HeaderText = "Delete";
            //delstudent.Text = "Delete";
            //delstudent.ButtonType = ButtonColumnType.PushButton;
            //delstudent.CommandName = "DelWebPage";
            //webpage_list.Columns.Add(delstudent);

            webpage_list.DataBind();
        }

        protected void CreateNewPage(object sender, EventArgs e)
        {
            Response.Redirect("CreateNewPage.aspx");
        }

        protected DataView WebPages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;

            DataColumn editcol = new DataColumn();
            editcol.ColumnName = "Edit";
            mytbl.Columns.Add(editcol);

            DataColumn delcol = new DataColumn();
            delcol.ColumnName = "Delete";
            mytbl.Columns.Add(delcol);

            

            foreach (DataRow row in mytbl.Rows)
            {
                row["Page Title"] =
                    "<a href=\"EditPageDetails.aspx?webpageid="
                    + row["webpageid"]
                    + "\">"
                    + row["Page Title"]
                    + "</a>";
                
                row["Edit"] = "<a href=\"EditPageDetails.aspx?webpageid=" + row["webpageid"] + "\"><span class='btn btn-success'>Edit</span></a>";
                row["Delete"] = "<a href=\"DeletePage.aspx?webpageid=" + row["webpageid"] + "\"><span class='btn btn-danger'>Delete</span></a>";
            }
            mytbl.Columns.Remove("webpageid");
            myview = mytbl.DefaultView;

            return myview;
        }

        protected void Delete_ItemCommand(object sender, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "DelWebPage")
            {
                //int rownum = Convert.ToInt32(e.CommandArgument);
                int webpageid = Convert.ToInt32(e.Item.Cells[0].Text);
                //assocstudentid = students_list.R
                DeleteWebpage(webpageid);
                //student_query.InnerHtml = "You tried to delete a student! "+studentid;
            }

        }

        protected void DeleteWebpage(int webpageid)
        {
            string basequery = "DELETE FROM webpagecontent WHERE webpageid=" + webpageid.ToString();
            manage_pages.DeleteCommand = basequery;
            manage_pages.Delete();

            basequery = "DELETE FROM WEBPAGES WHERE webpageid=" + webpageid.ToString();
            manage_pages.DeleteCommand = basequery;
            manage_pages.Delete();
        }
    }
}