﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DisplayWebPage.aspx.cs" Inherits="FinalProject5101.DisplayWebPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="page_query1" ConnectionString="<%$ ConnectionStrings:webpagedb_con %>">
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadingContentPlaceHolder" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <h1 runat="server" ID="WebPageTitle"></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <em>Author: <span runat="server" ID="WebPageAuthor"></span></em>
        </div>
        <div class="col-lg-6 col-md-6">
            <em>Page Created: <span runat="server" ID="WebPageCreatedOn"></span></em>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="UserControlContentPlaceHolder" runat="server">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <hr/>
        </div>
    </div>
    <div class="row">
         <div class="col-lg-12 col-md-12 aman" runat="server" ID="WebPageContent1" >
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <br />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 aman" runat="server" ID="WebPageContent2">
        </div>
    </div>
</asp:Content>
