﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPageDetails.aspx.cs" Inherits="FinalProject5101.EditPageDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="page_query" ConnectionString="<%$ ConnectionStrings:webpagedb_con %>">
    </asp:SqlDataSource>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadingContentPlaceHolder" runat="server">
    <h3 id="heading" runat="server"></h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="UserControlContentPlaceHolder" runat="server">
    <div class="inputrow">
        <asp:Label runat="server">Page Title:</asp:Label>
        <asp:TextBox ID="pageTitle" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="pageTitle"
            ErrorMessage="Enter a page title">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <asp:Label runat="server">Author Name:</asp:Label>
        <asp:TextBox ID="pageAuthorName" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="pageAuthorName"
            ErrorMessage="Enter an author name">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <asp:Label runat="server">Page Content 1:</asp:Label>
        <asp:TextBox ID="PageContentOne" runat="server" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="PageContentOne"
            ErrorMessage="Enter a page title">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <asp:Label runat="server">Page Content 2:</asp:Label>
        <asp:TextBox ID="PageContentTwo" runat="server" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="PageContentTwo"
            ErrorMessage="Enter an author name">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <asp:Label runat="server">Page Status:</asp:Label>
        <asp:CheckBox ID="pageStatus" runat="server" Text="Publish" />
    </div>
    <div class="inputrow">
        <a href="ManagePages.aspx"><span class='btn btn-default'>Back</span></a>
        <asp:Button runat="server" CssClass="btn btn-primary" ID="submitButton" OnClick="EditPage" Text="Update Page"/>        
    </div>
    <div id="result" runat="server"></div>
</asp:Content>
