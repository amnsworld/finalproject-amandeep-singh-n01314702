﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManagePages.aspx.cs" Inherits="FinalProject5101.ManagePages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadingContentPlaceHolder" runat="server">
    <asp:SqlDataSource runat="server" id="manage_pages" ConnectionString="<%$ ConnectionStrings:webpagedb_con %>"></asp:SqlDataSource>
    <h2>Manage Pages</h2>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="UserControlContentPlaceHolder" runat="server">
    <asp:Button runat="server" ID="createnewpagebutton" Text="Create New Page" OnClick="CreateNewPage"/>
   <asp:DataGrid id="webpage_list"
        runat="server" >
    </asp:DataGrid>
</asp:Content>
