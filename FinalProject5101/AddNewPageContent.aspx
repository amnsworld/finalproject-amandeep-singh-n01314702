﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddNewPageContent.aspx.cs" Inherits="FinalProject5101.AddNewPageContent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadingContentPlaceHolder" runat="server">   
    <asp:SqlDataSource runat="server" id="page_content" ConnectionString="<%$ ConnectionStrings:webpagedb_con %>"></asp:SqlDataSource>
    <h2 id="PageTitle" runat="server"></h2>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="UserControlContentPlaceHolder" runat="server">
    <div class="inputrow">
        <div class="row">
            <div class="col-lg-2 col-md-2">
               <asp:Label runat="server" ID="newpagecontent1">Page Content 1:</asp:Label>
            </div>
            <div class="col-lg-10 col-md-10">
                <asp:TextBox ID="PageContentOne" runat="server" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
                <%--<asp:RequiredFieldValidator runat="server"
                    ControlToValidate="PageContentOne"
                    ErrorMessage="Enter Page Content 1"
                    forecolor="Red">
                </asp:RequiredFieldValidator>--%>
            </div>
        </div>        
    </div>

    <div class="inputrow">
        <div class="row">
            <div class="col-lg-2 col-md-2">
               <asp:Label runat="server" ID="newpagecontent2">Page Content 2:</asp:Label>
            </div>
            <div class="col-lg-10 col-md-10">
                <asp:TextBox ID="PageContentTwo" runat="server" TextMode="multiline" Columns="50" Rows="5"></asp:TextBox>
               <%-- <asp:RequiredFieldValidator runat="server"
                    ControlToValidate="PageContentTwo"
                    ErrorMessage="Enter Page Content 2"
                    forecolor="Red">
                </asp:RequiredFieldValidator>--%>
            </div>
        </div>        
    </div>

    <div class="inputrow">
        <div class="row">
            <div class="col-lg-2 col-md-2">
                <asp:Label runat="server">Page Status:</asp:Label>
            </div>
            <div class="col-lg-10 col-md-10">
                <asp:CheckBox ID="pageStatus" runat="server" Text="Publish" />
            </div>
        </div>
    </div>
    <div class="submitData">
        <asp:Button runat="server" ID="submitNewPageContent" OnClick="NewPageContentSubmitted" Text="Submit"/>
    </div>
</asp:Content>
