﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FinalProject5101
{
    public partial class DeletePage : System.Web.UI.Page
    {
        public int webpageid
        {
            get { return Convert.ToInt32(Request.QueryString["webpageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            string basequery = "DELETE FROM webpagecontent WHERE webpageid=" + webpageid.ToString();
            deletepage_query.DeleteCommand = basequery;
            deletepage_query.Delete();

            basequery = "DELETE FROM WEBPAGES WHERE webpageid=" + webpageid.ToString();
            deletepage_query.DeleteCommand = basequery;
            deletepage_query.Delete();
            result.InnerHtml = "<div class='alert alert-success' role='alert'>Success! Page has been deleted. </div><br><a href='ManagePages.aspx'><span class='btn btn-primary'>Back</span></a>";
        }
    }
}