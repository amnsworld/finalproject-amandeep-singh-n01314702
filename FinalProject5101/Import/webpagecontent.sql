﻿CREATE TABLE "WEBPAGECONTENT" 
   (	
   "webpagecontentid" BIGINT IDENTITY(1,1) PRIMARY KEY, 
   "webpageid" BIGINT REFERENCES webpages(webpageid), 
   "webpagecontentone" VARCHAR(MAX), 
   "webpagecontenttwo" VARCHAR(MAX), 
   "webpagestatus" VARCHAR(10)
   );