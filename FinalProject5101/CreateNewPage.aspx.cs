﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace FinalProject5101
{
    public partial class CreateNewPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private string baseInsertQuery = "INSERT INTO WEBPAGES(pagetitle,pageauthor,createdon,modifiedon) VALUES";

        protected void NewPageSubmitted(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                //if there is any data invalid or there is any exception, we will stop further processing and throw the appropriate exception
                return;
            }

            //get the user input values
            string PageTitle = pageTitle.Text.ToString();
            string AuthorName = pageAuthorName.Text.ToString();           

            string CreatedOn = DateTime.Today.ToString("dd/MM/yyyy");
            string UpdatedOn = DateTime.Today.ToString("dd/MM/yyyy");


            baseInsertQuery += "('" +
                PageTitle + "','" + AuthorName + "',convert(DATETIME,'" + CreatedOn + "',103),convert(DATETIME,'" + UpdatedOn + "',103))";
            
            /* Referenced https://docs.microsoft.com/en-us/dotnet/framework/data/adonet/obtaining-a-single-value-from-a-database 
             & https://stackoverflow.com/questions/23976683/asp-net-button-to-redirect-to-another-page 
             for passing parameter to another page and opening new page. */

            Int32 newPageID = 0;
            string sql = baseInsertQuery + "; SELECT CAST(scope_identity() AS int)";
            string constr = ConfigurationManager.ConnectionStrings["webpagedb_con"].ConnectionString;

            using (SqlConnection conn = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand(sql, conn);
                try
                {
                    conn.Open();
                    newPageID = (Int32)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }

            //Session["NewPageID"] = newPageID;
            Response.Redirect("AddNewPageContent.aspx?NewPageID=" + newPageID);

        }
    }
}