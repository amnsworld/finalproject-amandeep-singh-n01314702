﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateNewPage.aspx.cs" Inherits="FinalProject5101.CreateNewPage" %>
<asp:Content ID="Content2" ContentPlaceHolderID="HeadingContentPlaceHolder" runat="server">
    <asp:SqlDataSource runat="server" id="insert_new_page" ConnectionString="<%$ ConnectionStrings:webpagedb_con %>"></asp:SqlDataSource>   
    <h3>Add New Page</h3>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="UserControlContentPlaceHolder" runat="server">
    <div class="inputrow">
        <asp:Label runat="server">Page Title:</asp:Label>
        <asp:TextBox ID="pageTitle" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="pageTitle"
            ErrorMessage="Enter a page title"
            forecolor="Red">
        </asp:RequiredFieldValidator>
    </div>
    <div class="inputrow">
        <asp:Label runat="server">Author Name:</asp:Label>
        <asp:TextBox ID="pageAuthorName" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="pageAuthorName"
            ErrorMessage="Enter an author name"
            forecolor="Red">
        </asp:RequiredFieldValidator>
    </div>
   <%-- <div class="inputrow">
        <asp:Label runat="server">Page Status:</asp:Label>
        <asp:CheckBox ID="pageStatus" runat="server" Text="Publish" />
    <asp:RequiredFieldValidator runat="server"
            ControlToValidate="pageName"
            ErrorMessage="Enter a page name">
        </asp:RequiredFieldValidator>
    </div>--%>

    <div class="submitData">
        <asp:Button runat="server" ID="submitNewPage" OnClick="NewPageSubmitted" Text="Submit"/>
    </div>
</asp:Content>
