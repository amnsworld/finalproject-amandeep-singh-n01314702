﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data.SqlClient;


namespace FinalProject5101
{
    public partial class AddNewPageContent : System.Web.UI.Page
    {
        public int NewWebPageID
        {
            get { return Convert.ToInt32(Request.QueryString["NewPageID"]); }
        }

        //private Int32 NewWebPageID = 0;
        private string baseInsertQuery = "INSERT INTO WEBPAGECONTENT(webpageid,webpagecontentone,webpagecontenttwo,webpagestatus) VALUES";
        private string baseSelectQuery = "SELECT PAGETITLE FROM WEBPAGES WHERE WEBPAGEID = ";
        private string constr = ConfigurationManager.ConnectionStrings["webpagedb_con"].ConnectionString;        

        protected void Page_Load(object sender, EventArgs e)
        {
           // NewWebPageID = (Int32)Session["NewPageID"];
            baseSelectQuery += NewWebPageID;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand(baseSelectQuery, conn);
                try
                {
                    conn.Open();
                    string abc =(string)cmd.ExecuteScalar();
                    PageTitle.InnerHtml = abc;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    conn.Close();
                }
            }
        }


        protected void NewPageContentSubmitted(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                //if there is any data invalid or there is any exception, we will stop further processing and throw the appropriate exception
                return;
            }

            //get the user input values
            string pagecontentone = PageContentOne.Text.ToString();
            string pagecontenttwo = PageContentTwo.Text.ToString();
            string Status = "UnPublished";
            if (pageStatus.Checked == true)
            {
                Status = "Published";
            }

            baseInsertQuery += "('" + NewWebPageID + "','" + pagecontentone + "','" + pagecontenttwo + "' ,'" + Status + "')";

            page_content.InsertCommand = baseInsertQuery;
            page_content.Insert();

            //using (SqlConnection conn = new SqlConnection(constr))
            //{
            //    SqlCommand cmd = new SqlCommand(baseInsertQuery, conn);
            //    try
            //    {
            //        conn.Open();
            //        cmd.ExecuteScalar();
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message);
            //    }
            //    finally
            //    {
            //        conn.Close();
            //    }
            //}

            Response.Redirect("ManagePages.aspx");
        }
    }
}